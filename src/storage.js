
const store = new Map();

export async function storeDiscordTokens(userId, tokens) {
  await store.set(`discord-${userId}`, tokens);
}

export async function getDiscordTokens(userId) {
  return store.get(`discord-${userId}`);
}

export async function storeFtTokens(userId, tokens) {
    await store.set(`ft-${userId}`, tokens);
}

export async function getFtTokens(userId) {
    return store.get(`ft-${userId}`);
}
