import mysql from "mysql2";
import config from './config.js';

function getConection() {
    const connection = mysql.createConnection({
        host: config.DB_HOST,
        user: config.DB_USER,
        password: config.DB_PASSWORD,
        database: config.DB_NAME,
    });
    connection.connect();
    return connection;
}

export async function getUserByDiscordId(discordId) {
    const connection = getConection();
    let user = await connection.execute('SELECT * FROM users WHERE discordid = ?', [discordId]);
    let res =  user.length > 0 ?  user[0][0] :  null;
    return await res;
}

export async function getUserBylogin(login) {
    const connection = getConection();
    let user = await connection.execute('SELECT * FROM users WHERE login = ?', [login]);
    let res =  user.length > 0 ?  user[0][0] :  null;
    return await res;
}

export async function userExists(login, discordId) {
    const connection = getConection();
    let user = await connection.execute('SELECT * FROM users WHERE login = ? OR discordid = ?', [login, discordId]);
    return user.length > 0;
}

export async function cresteUser(login, discordId) {
    const connection = getConection();
    let user = await connection.execute('INSERT INTO users (login, discordid, verified) VALUES (?, ?, true)', [login, discordId]);
    return await user;
}