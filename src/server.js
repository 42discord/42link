import express from 'express';
import cookieParser from 'cookie-parser';

import config from './config.js';
import * as discord from './discord.js';
import * as storage from './storage.js';
import {getOAuthUrl} from "./ft.js";
import * as ft from "./ft.js";
import * as my from "./mysql.js";
import {cresteUser, getUserByDiscordId} from "./mysql.js";

/**
 * Main HTTP server used for the bot.
 */

 const app = express();
 app.use(cookieParser(config.COOKIE_SECRET));

 /**
  * Just a happy little route to show our server is up.
  */
 app.get('/', (req, res) => {
   res.send('👋');
 });

app.get('/linked-role', async (req, res) => {
    const { url, state } = ft.getOAuthUrl();

    res.cookie('FtclientState', state, { maxAge: 1000 * 60 * 5, signed: true });

    res.redirect(url);
});

/**
 * Route configured in the Discord developer console, the redirect Url to which
 * the user is sent after approving the bot for their Discord account. This
 * completes a few steps:
 * 1. Uses the code to acquire Discord OAuth2 tokens
 * 2. Uses the Discord Access Token to fetch the user profile
 * 3. Stores the OAuth2 Discord Tokens in Redis / Firestore
 * 4. Lets the user know it's all good and to go back to Discord
 */
 app.get('/discord-oauth-callback', async (req, res) => {
  try {
    // 1. Uses the code and state to acquire Discord OAuth2 tokens
    const code = req.query['code'];
    const discordState = req.query['state'];

    // make sure the state parameter exists
    const { clientState } = req.signedCookies;
    if (clientState !== discordState) {
      console.error('State verification failed.');
      return res.sendStatus(403);
    }

    const tokens = await discord.getOAuthTokens(code);

    // 2. Uses the Discord Access Token to fetch the user profile
    const meData = await discord.getUserData(tokens);
    const userId = meData.user.id;

    const { FtUserLogin } = req.signedCookies

    const test = await my.userExists(FtUserLogin, userId);
    if(!test) {
        await my.cresteUser(FtUserLogin, userId);
    }

    await storage.storeDiscordTokens(userId, {
      access_token: tokens.access_token,
      refresh_token: tokens.refresh_token,
      expires_at: Date.now() + tokens.expires_in * 1000,
    });

    // 3. Update the users metadata, assuming future updates will be posted to the `/update-metadata` endpoint
    await updateMetadata(userId);

    res.send('You did it!  Now go back to Discord.');
  } catch (e) {
    console.error(e);
    res.sendStatus(500);
  }
});

 app.get('/ft-oauth-callback', async (req, res) => {
     try {
         const code = req.query['code'];
         const ftState = req.query['state'];

         const {FtclientState} = req.signedCookies;
         if (FtclientState !== ftState) {
             console.error('State verification failed.');
             return res.sendStatus(403);
         }


         const tokens = await ft.getOAuthToken(code);

         const meData = await ft.getUserData(tokens);

         const userId = meData.id;
         const UserLogin = meData.login;

         const cursus = meData.cursus_users;
         const cursus_ids = cursus.map(cursus => cursus.cursus_id);
         const have_cursus = cursus_ids.includes(1) || cursus_ids.includes(21);

         if(!have_cursus){
             console.error('You are not a student');
             return res.sendStatus(403);
         }

         await storage.storeFtTokens(userId, {
             access_token: tokens.access_token,
             refresh_token: tokens.refresh_token,
             expires_at: Date.now() + tokens.expires_in * 1000,
         });

         res.cookie('FtUserId', userId, { maxAge: 1000 * 60 * 5, signed: true });
         res.cookie('FtUserLogin', UserLogin, { maxAge: 1000 * 60 * 5, signed: true });

         const { url, state } = discord.getOAuthUrl();

         res.cookie('clientState', state, { maxAge: 1000 * 60 * 5, signed: true });

         res.redirect(url);

     } catch (e) {
         console.error(e);
         res.sendStatus(500);
     }
 });
/**
 * Example route that would be invoked when an external data source changes. 
 * This example calls a common `updateMetadata` method that pushes static
 * data to Discord.
 */
 app.post('/update-metadata', async (req, res) => {
  try {
    const userId = req.body.userId;
    await updateMetadata(userId)

    res.sendStatus(204);
  } catch (e) {
    res.sendStatus(500);
  }
});

/**
 * Given a Discord UserId, push static make-believe data to the Discord 
 * metadata endpoint. 
 */
async function updateMetadata(userId) {
  // Fetch the Discord tokens from storage
  const tokens = await storage.getDiscordTokens(userId);
    
  let metadata = {};
  try {
    // Fetch the new metadata you want to use from an external source. 
    // This data could be POST-ed to this endpoint, but every service
    // is going to be different.  To keep the example simple, we'll
    // just generate some random data. 
    metadata = {
      student: 1,
    };
  } catch (e) {
    e.message = `Error fetching external data: ${e.message}`;
    console.error(e);
    // If fetching the profile data for the external service fails for any reason,
    // ensure metadata on the Discord side is nulled out. This prevents cases
    // where the user revokes an external app permissions, and is left with
    // stale linked role data.
  }

  // Push the data to Discord.
  await discord.pushMetadata(userId, tokens, metadata);
}


const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
