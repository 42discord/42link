import fetch from 'node-fetch';
import config from './config.js';
import crypto from "crypto";


export function getOAuthUrl() {
    const state = crypto.randomUUID();

    const url = new URL('https://api.intra.42.fr/oauth/authorize');
    url.searchParams.set('client_id', config.FT_UID);
    url.searchParams.set('redirect_uri', config.FT_REDIRECT_URI);
    url.searchParams.set('response_type', 'code');
    url.searchParams.set('scope', 'public');
    url.searchParams.set('state', state);
    return { state, url: url.toString() };
}

export async function getOAuthToken(code) {
    const url = 'https://api.intra.42.fr/oauth/token'
    const body = new URLSearchParams({
        grant_type: 'authorization_code',
        client_id: config.FT_UID,
        client_secret: config.FT_SECRET,
        code,
        redirect_uri: config.FT_REDIRECT_URI,
    });

    const response = await fetch(url, {
        body,
        method: 'POST'
    });

    if (response.ok) {
        return await response.json();
    } else {
        throw new Error(`Error fetching OAuth tokens: [${response.status}] ${response.statusText}`);
    }
}

export async function getUserData(tokens) {
    const url = 'https://api.intra.42.fr/v2/me';
    const response = await fetch(url, {
        headers: {
            Authorization: `Bearer ${tokens.access_token}`,
        },
    });

    if (response.ok) {
        return await response.json();
    } else {
        throw new Error(`Error fetching user data: [${response.status}] ${response.statusText}`);
    }
}